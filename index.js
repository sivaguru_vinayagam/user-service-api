const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.json ([
    {
        name: 'siva',
        email: 'siva@gmail.com'
    },
    {
        name: 'luc',
        email: 'luc@hotmail.com'
    },{
        name: 'eva',
        email: 'eva@free.fr'
    },
    {
        name: 'elisa',
        email: 'elisa@yahoo.fr'
    }

]))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})